import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AmountFieldComponent } from './amount-field/amount-field.component';
import { InstrumentComponent } from './instrument/instrument.component';
import { AmountField1Component } from './amount-field1/amount-field1.component';

@NgModule({
  declarations: [
    AppComponent,
    AmountFieldComponent,
    InstrumentComponent,
    AmountField1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
