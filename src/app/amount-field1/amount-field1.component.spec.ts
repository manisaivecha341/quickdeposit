import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountField1Component } from './amount-field1.component';

describe('AmountField1Component', () => {
  let component: AmountField1Component;
  let fixture: ComponentFixture<AmountField1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmountField1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountField1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
