import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl} from '@angular/forms';

@Component({
  selector: 'app-instrument',
  templateUrl: './instrument.component.html',
  styleUrls: ['./instrument.component.css']
})
export class InstrumentComponent implements OnInit {

  instrumentForm : FormGroup;
  constructor() { }

  ngOnInit() {
    this.instrumentForm=new FormGroup({
      accountNumber : new FormControl(),
      accountCode : new FormControl()
    });

  }

}
