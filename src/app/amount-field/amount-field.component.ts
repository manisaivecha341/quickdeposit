import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl} from '@angular/forms';

@Component({
  selector: 'app-amount-field',
  templateUrl: './amount-field.component.html',
  styleUrls: ['./amount-field.component.css']
})
export class AmountFieldComponent implements OnInit {
  amountForm : FormGroup;
  accountCurrency : String;
  constructor() { }

  ngOnInit() {
    this.accountCurrency = "GBP";
    this.amountForm = new FormGroup({
      accountCurrency : new FormControl(),
      amount : new FormControl()
    });

  }

  onSubmit() : void{
    console.log(this.amountForm);
  }

}
